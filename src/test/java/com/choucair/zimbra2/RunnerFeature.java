package com.choucair.zimbra2;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
//@CucumberOptions(features="src/test/resources/features")
@CucumberOptions(features="src/test/resources/features/zimbra/mensajes/NuevaSucursal.feature", tags="@creacionsucursal")
public class RunnerFeature {

}
