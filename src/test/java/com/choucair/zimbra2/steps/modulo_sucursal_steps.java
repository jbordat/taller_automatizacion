package com.choucair.zimbra2.steps;

import com.choucair.zimbra2.pageObjet.modulo_sucursal_pageObjet;

import net.thucydides.core.annotations.Step;

public class modulo_sucursal_steps {

	 modulo_sucursal_pageObjet modulo;
	
	 @Step
	public void ingresar_al_formulario_de_nueva_sucursal() {
		 modulo.Archivo();
		 modulo.Sucursal();
		 modulo.Nuevo();
		 
	}
}
