package com.choucair.zimbra2.steps;

import com.choucair.zimbra2.pageObjet.nueva_sucursal_pageObjet;

import net.thucydides.core.annotations.Step;

public class nueva_sucursal_steps {
	
	nueva_sucursal_pageObjet nueva;

	@Step
public void diligenciar_el_formulario_de_la_vista_general(String arg1, String arg2, String arg3, String arg4)  {
		
		nueva.Descripcion(arg1);
		nueva.AgregarPre(arg2);
		nueva.Prefijo(arg3);
		nueva.LNumero(arg4);
	}
	@Step
public void diligenciar_el_formulario_de_la_vista_impresion(String arg1, String arg2) {
	   nueva.Btn_impresion();
	   nueva.imagen1(arg1);
	   nueva.imagen2(arg2);
	   
}
	@Step
public void diligenciar_el_formulario_de_numeracion_de_documentos(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, String arg7, String arg8, String arg9, String arg10, String arg11, String arg12, String arg13, String arg14, String arg15, String arg16, String arg17, String arg18, String arg19, String arg20, String arg21, String arg22, String arg23, String arg24, String arg25, String arg26, String arg27) {
    nueva.Btn_documentacion();
    nueva.documentacion(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20, arg21, arg22, arg23, arg24, arg25, arg26, arg27);
}
	@Step
public void guardar_la_nueva_sucursal(){
    nueva.Btn_guardar();
}
	@Step
public void validar_la_creacion_de_la_nueva_sucursal(String arg1) {
	  nueva.Validarsucursal(arg1);
}



}
