package com.choucair.zimbra2.steps;

import com.choucair.zimbra2.pageObjet.redactarmensajepageObjet;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Step;

public class redactarmensajesteps {
	
	redactarmensajepageObjet redactarmensajepageObjet1;
	
	@Step
	public void diligenciar_el_formulario_con_encabezado_cuerpo_y_correo(String mensaje, String cuerpo, String correo) {
		redactarmensajepageObjet1.Para(correo);
		redactarmensajepageObjet1.Asunto(mensaje);
		redactarmensajepageObjet1.Cuerpo(cuerpo);
	}
	@Step
	public void envio_el_mensaje() {
		redactarmensajepageObjet1.Enviar();
	}
	@Step
	public void validamos_el_envio_con_encabezado_cuerpo_y_correo(String mensaje, String cuerpo, String correo) {
		redactarmensajepageObjet1.MEnviado();
		redactarmensajepageObjet1.Multimo();
		//redactarmensajepageObjet1.ValidarCorreo(correo, mensaje, cuerpo);
	}

}
