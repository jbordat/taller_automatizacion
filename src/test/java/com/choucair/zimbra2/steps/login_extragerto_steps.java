package com.choucair.zimbra2.steps;

import com.choucair.zimbra2.pageObjet.login_extragerto_pageObjet;

import net.thucydides.core.annotations.Step;

public class login_extragerto_steps {

	login_extragerto_pageObjet login;
	
	@Step
	public void ingresar_a_la_ruta_de(String arg1) {
		login.openAt(arg1);
	}
	@Step
	public void loguearse_con_otro_usuario_y_otra_contrasena(String usuario, String pass) {
		login.Usuario1(usuario);
		login.Contrasena1(pass);
		login.Ingresar1();
	}
	@Step
	public void loguearse_con_usuario_y_contrasena(String usuario, String pass) {
		login.Usuario(usuario);
		login.Contrasena(pass);
		login.Ingresar();
	}
	
	
	
}
