package com.choucair.zimbra2.steps;

import com.choucair.zimbra2.pageObjet.loginpageObjet;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Step;

public class loginsteps {

	loginpageObjet loginpageObjet1;
	
	@Step
   public void ingresar_al_aplicativo_zimbra(String strurl) {
		loginpageObjet1.openAt(strurl);
	}
	@Step
	public void loguearse_con_usuario_y_contraseña(String usuario, String contrasena) {
		loginpageObjet1.Usuario(usuario);
		loginpageObjet1.Contrasena(contrasena);
		loginpageObjet1.Ingresar();
	}
}
