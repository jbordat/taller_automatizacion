package com.choucair.zimbra2.definitions;


import com.choucair.zimbra2.steps.menusteps;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class menudefinition {

	@Steps
	menusteps menusteps1;
	
	@When("^Ingresar al formulario de mensaje$")
	public void Ingresar_al_formulario_de_mensaje() {
		menusteps1.Ingresar_al_formulario_de_mensaje();
	}
}
