package com.choucair.zimbra2.definitions;

import com.choucair.zimbra2.steps.login_extragerto_steps;

import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class login_extragerto_definition {

	@Steps
	login_extragerto_steps login;
	
	
	@Given("^ingresar a la ruta de \"([^\"]*)\"$")
	public void ingresar_a_la_ruta_de(String arg1) {
		login.ingresar_a_la_ruta_de(arg1);
	}
	@Given("^loguearse con otro usuario \"([^\"]*)\" y otra contrasena \"([^\"]*)\"$")
	public void loguearse_con_otro_usuario_y_otra_contrasena(String usuario, String pass) {
		login.loguearse_con_otro_usuario_y_otra_contrasena(usuario, pass);
	}
	@Given("^loguearse con usuario \"([^\"]*)\" y contrasena \"([^\"]*)\"$")
	public void loguearse_con_usuario_y_contrasena(String usuario, String pass) {
		login.loguearse_con_usuario_y_contrasena(usuario,pass);
	}
	
	
}
