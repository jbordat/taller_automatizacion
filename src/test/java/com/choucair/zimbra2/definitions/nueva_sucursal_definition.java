package com.choucair.zimbra2.definitions;

import com.choucair.zimbra2.steps.nueva_sucursal_steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class nueva_sucursal_definition {
	
	@Steps
	nueva_sucursal_steps modelo;
	
	@When("^diligenciar el formulario de la vista general \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
	public void diligenciar_el_formulario_de_la_vista_general(String arg1, String arg2, String arg3, String arg4)  {
	    modelo.diligenciar_el_formulario_de_la_vista_general(arg1, arg2, arg3, arg4);
	}

@When("^diligenciar el formulario de la vista impresion \"([^\"]*)\", \"([^\"]*)\"$")
public void diligenciar_el_formulario_de_la_vista_impresion(String arg1, String arg2) {
   modelo.diligenciar_el_formulario_de_la_vista_impresion(arg1, arg2);
}

@When("^diligenciar el formulario de numeracion de documentos \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\"$")
public void diligenciar_el_formulario_de_numeracion_de_documentos(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, String arg7, String arg8, String arg9, String arg10, String arg11, String arg12, String arg13, String arg14, String arg15, String arg16, String arg17, String arg18, String arg19, String arg20, String arg21, String arg22, String arg23, String arg24, String arg25, String arg26, String arg27) {
    modelo.diligenciar_el_formulario_de_numeracion_de_documentos(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20, arg21, arg22, arg23, arg24, arg25, arg26, arg27);
}

@When("^Guardar la nueva sucursal$")
public void guardar_la_nueva_sucursal(){
 modelo.guardar_la_nueva_sucursal();   
}

@Then("^Validar la creacion de la nueva sucursal \"([^\"]*)\"$")
public void validar_la_creacion_de_la_nueva_sucursal(String arg1) {
   modelo.validar_la_creacion_de_la_nueva_sucursal(arg1);
}

}
