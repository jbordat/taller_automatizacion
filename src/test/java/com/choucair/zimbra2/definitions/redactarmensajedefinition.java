package com.choucair.zimbra2.definitions;

import com.choucair.zimbra2.steps.redactarmensajesteps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class redactarmensajedefinition {

	@Steps
	redactarmensajesteps redactarmensajesteps1;
	
	@When("^diligenciar el formulario con encabezado \"([^\"]*)\", cuerpo \"([^\"]*)\" y correo \"([^\"]*)\"$")
	public void diligenciar_el_formulario_con_encabezado_cuerpo_y_correo(String mensaje, String cuerpo, String correo) {
		redactarmensajesteps1.diligenciar_el_formulario_con_encabezado_cuerpo_y_correo(mensaje, cuerpo,correo);
	}
	@When("^envio el mensaje$")
	public void envio_el_mensaje() {
		redactarmensajesteps1.envio_el_mensaje();
	}
@Then("^validamos el envio con encabezado \"([^\"]*)\", cuerpo \"([^\"]*)\" y correo \"([^\"]*)\"$")	
public void validamos_el_envio_con_encabezado_cuerpo_y_correo(String mensaje, String cuerpo, String correo) {
	redactarmensajesteps1.validamos_el_envio_con_encabezado_cuerpo_y_correo(mensaje, cuerpo, correo);
	}
	
}
