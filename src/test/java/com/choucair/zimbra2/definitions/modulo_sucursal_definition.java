package com.choucair.zimbra2.definitions;

import com.choucair.zimbra2.steps.modulo_sucursal_steps;

import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class modulo_sucursal_definition {

	@Steps
	modulo_sucursal_steps modulo1;
	
	@When("^Ingresar al formulario de nueva sucursal$")
	public void ingresar_al_formulario_de_nueva_sucursal() {
	    modulo1.ingresar_al_formulario_de_nueva_sucursal();
	}
}
