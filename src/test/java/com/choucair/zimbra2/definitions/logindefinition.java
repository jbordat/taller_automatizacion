package com.choucair.zimbra2.definitions;

import com.choucair.zimbra2.steps.loginsteps;
import cucumber.api.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class logindefinition {

	@Steps
	loginsteps loginsteps1;
	
	@Given("^ingresar al aplicativo zimbra \"([^\"]*)\"$")
	public void ingresar_al_aplicativo_zimbra(String strurl) {
		loginsteps1.ingresar_al_aplicativo_zimbra(strurl);
	}
	@Given("^loguearse con usuario \"([^\"]*)\" y contraseña \"([^\"]*)\"$")
	public void loguearse_con_usuario_y_contraseña(String usuario, String contrasena) {
		loginsteps1.loguearse_con_usuario_y_contraseña(usuario, contrasena);
	}
}
