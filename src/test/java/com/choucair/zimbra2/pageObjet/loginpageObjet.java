package com.choucair.zimbra2.pageObjet;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;


public class loginpageObjet extends PageObject {

	@FindBy(id="username")
	public WebElementFacade TxtUsuario;
	
	@FindBy(id="password")
	public WebElementFacade TxtPass;
	
	@FindBy(xpath="/html/body/div/div[1]/form/table/tbody/tr[3]/td[2]/input[1]")
	public WebElementFacade BtnIngresar;
	
	public void Usuario(String Zimbra) {
		TxtUsuario.click();
		TxtUsuario.clear();
		TxtUsuario.sendKeys(Zimbra);
	}
	
	public void Contrasena(String Zimbra) {
		TxtPass.click();
		TxtPass.clear();
		TxtPass.sendKeys(Zimbra);
	}
	
	public void Ingresar() {
		BtnIngresar.click();
		
	}
}
