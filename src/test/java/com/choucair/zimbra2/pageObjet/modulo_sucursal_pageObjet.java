package com.choucair.zimbra2.pageObjet;

import org.openqa.selenium.WebDriver;

import com.ibm.icu.impl.duration.TimeUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class modulo_sucursal_pageObjet extends PageObject{
	
	
	
	@FindBy(xpath="//A[@id='item_1']")
	public WebElementFacade BtnArchivo;
	@FindBy(xpath="//A[@id='item_18']")
	public WebElementFacade BtnSucursal;
	@FindBy(xpath="//*[@id=\"sc_b_new_top\"]")
	public WebElementFacade BtnNuevo;

	public void Archivo() {
		System.out.print("ingresa a archivo");
		try {
		
			BtnArchivo.click();
		} catch (Exception e) {
			System.out.print(e);
			
		}
	
	}
	public void Sucursal() {
		BtnSucursal.click();
	}
	
	public void Nuevo() {
		
		System.out.println("si entro aca");
		try {
			getDriver().switchTo().frame(4);
			getDriver().switchTo().frame(0);
			BtnNuevo.click();
			
			System.out.println("presiono el boton");
		} catch (Exception e) {
			System.out.println("el error es "+ e);
		}
	}
}
