package com.choucair.zimbra2.pageObjet;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class login_extragerto_pageObjet extends PageObject {

	@FindBy(id="cred_userid_inputtext")
	public WebElementFacade TxtUsuario1;
	
	@FindBy(id="cred_password_inputtext")
	public WebElementFacade TxtPass1;
	
	@FindBy(id="submit")
	public WebElementFacade BtnIngresar1;
	
	@FindBy(id="id_sc_field_login")
	public WebElementFacade TxtUsuario;
	
	@FindBy(id="id_sc_field_pswd")
	public WebElementFacade TxtPass;
	
	@FindBy(xpath="//*[@id=\"login1\"]/form/div[3]/button")
	public WebElementFacade BtnIngresar;
	
	public void Usuario1(String Zimbra) {
		TxtUsuario1.click();
		TxtUsuario1.clear();
		TxtUsuario1.sendKeys(Zimbra);
	}
	
	public void Contrasena1(String Zimbra) {
		TxtPass1.click();
		TxtPass1.clear();
		TxtPass1.sendKeys(Zimbra);
	}
	
	public void Ingresar1() {
		BtnIngresar1.click();
		
	}
	
	public void Usuario(String Zimbra) {
		TxtUsuario.click();
		TxtUsuario.clear();
		TxtUsuario.sendKeys(Zimbra);
	}
	
	public void Contrasena(String Zimbra) {
		TxtPass.click();
		TxtPass.clear();
		TxtPass.sendKeys(Zimbra);
	}
	
	public void Ingresar() {
		BtnIngresar.click();
		
	}
	
}
