package com.choucair.zimbra2.pageObjet;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class menupageObjet extends PageObject {

	@FindBy(id="zb__CLV__NEW_MENU")
	public WebElementFacade TxtNuevoMensaje;
	
	public void NuevoMensaje() {
		TxtNuevoMensaje.click();
	
	}
}
