package com.choucair.zimbra2.pageObjet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class redactarmensajepageObjet extends PageObject {
	
	@FindBy(id="zv__COMPOSE1_to_control")
	public WebElementFacade TxtPara;

	@FindBy(id="zv__COMPOSE1_subject_control")
	public WebElementFacade TxtAsunto;
	
	@FindBy(id="textarea_DWT57")
	public WebElementFacade TxtCuerpo;
	
	@FindBy(id="zb__COMPOSE1__SEND")
	public WebElementFacade BtnEnviar;
	
	@FindBy(id="zti__main_Mail__5_textCell")
	public WebElementFacade BtnMEnviado;
	
	@FindBy(id="zlif__CLV__678__pa__0")
	public WebElementFacade TxtEcorreo;
	
	@FindBy(id="zlif__CLV__687__su")
	public WebElementFacade TxtEAsunto;
	
	@FindBy(xpath="//div[2]/table/tbody/tr[1]/td[7]")
	public WebElementFacade BtnEMensaje;
	
	public void Asunto(String Zimbra) {
		TxtAsunto.click();
		TxtAsunto.clear();
		TxtAsunto.sendKeys(Zimbra);
	}
	
	public void Para(String Zimbra) {
		TxtPara.typeAndEnter(Zimbra);
	}
	
	public void Cuerpo(String Zimbra) {
		TxtCuerpo.click();
		TxtCuerpo.clear();
		TxtCuerpo.sendKeys(Zimbra);
	}
	
	public void Enviar() {
		BtnEnviar.click();
	}
	
	public void MEnviado() {
		BtnMEnviado.click();
	}
	public void Multimo() {
		BtnEMensaje.click();
		
	}
	public void ValidarCorreo(String Ecorreo, String Easunto, String Ecuerpo) {
		try {
			String CorreoE=TxtEcorreo.getText();
			assertThat(Ecorreo, containsString(CorreoE));
			
			String AsuntoE=TxtEAsunto.getText();
			String completo= Easunto+"-"+Ecuerpo;
			assertThat(completo, containsString(AsuntoE));
			//fail("");
			//assertEquals(Ecorreo, CorreoE);
		} catch (AssertionError e) {
			
		}
	
	}
}
