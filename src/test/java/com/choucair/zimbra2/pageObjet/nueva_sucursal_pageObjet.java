package com.choucair.zimbra2.pageObjet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.openqa.selenium.WebDriver;


import com.ibm.icu.impl.duration.TimeUnit;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class nueva_sucursal_pageObjet extends PageObject {

	
	
	@FindBy(xpath="//*[@id=\"id_sc_field_descripcion\"]")
	public WebElementFacade Txtdescripcion;
	@FindBy(xpath="//*[@id=\"idAjaxRadio_sinprefijo\"]/table/tbody/tr/td[2]/input")
	public WebElementFacade Txtagre_prefijo;
	@FindBy(xpath="//*[@id=\"id_sc_field_prefijo\"]")
	public WebElementFacade Txtprefijo;
	@FindBy(xpath="//*[@id=\"id_sc_field_largo\"]")
	public WebElementFacade TxtLNumero;
	
	@FindBy(xpath="//*[@id=\"id_form_sucursales_form1\"]/a")
	public WebElementFacade Btnimpresion;
	

	@FindBy(xpath="//*[@id=\"mceu_14\"]/button")
	public WebElementFacade Btnicon1;
	@FindBy(xpath="//*[@id=\"mceu_59-inp\"]")
	public WebElementFacade Btnurl1;
	@FindBy(xpath="//*[@id=\"mceu_67\"]/button")
	public WebElementFacade Btn1;
	
	
	@FindBy(xpath="//*[@id=\"mceu_42\"]/button")
	public WebElementFacade Btnicon2;
	@FindBy(xpath="//*[@id=\"mceu_75-inp\"]")
	public WebElementFacade Btnurl2;
	@FindBy(xpath="//*[@id=\"mceu_83\"]/button")
	public WebElementFacade Btn2;
	
	@FindBy(xpath="//*[@id=\"id_form_sucursales_form2\"]/a")
	public WebElementFacade Btndocumentos;
	
	@FindBy(xpath="//*[@id=\"id_sc_field_orden_de_compra\"]")
	public WebElementFacade lbl1;
	@FindBy(xpath="//*[@id=\"id_sc_field_entrada\"]")
	public WebElementFacade lbl2;
	@FindBy(xpath="//*[@id=\"id_sc_field_factura_de_compra\"]")
	public WebElementFacade lbl3;
	@FindBy(xpath="//*[@id=\"id_sc_field_documento_equivalente\"]")
	public WebElementFacade lbl4;
	@FindBy(xpath="//*[@id=\"id_sc_field_devolucion_de_compra\"]")
	public WebElementFacade lbl5;
	@FindBy(xpath="//*[@id=\"id_sc_field_comprobante_de_egreso\"]")
	public WebElementFacade lbl6;
	
	@FindBy(xpath="//*[@id=\"id_sc_field_cotizacion\"]")
	public WebElementFacade lbl7;
	@FindBy(xpath="//*[@id=\"id_sc_field_pedido\"]")
	public WebElementFacade lbl8;
	@FindBy(xpath="//*[@id=\"id_sc_field_remision\"]")
	public WebElementFacade lbl9;
	@FindBy(xpath="//*[@id=\"id_sc_field_factura_de_venta\"]")
	public WebElementFacade lbl10;
	@FindBy(xpath="//*[@id=\"id_sc_field_devolucion_de_venta\"]")
	public WebElementFacade lbl11;
	@FindBy(xpath="//*[@id=\"id_sc_field_recibo_de_caja\"]")
	public WebElementFacade lbl12;
	
	@FindBy(xpath="//*[@id=\"id_sc_field_ajuste\"]")
	public WebElementFacade lbl13;
	@FindBy(xpath="//*[@id=\"id_sc_field_cuenta_de_cobro\"]")
	public WebElementFacade lbl14;
	@FindBy(xpath="//*[@id=\"id_sc_field_comprobante_de_contabilidad\"]")
	public WebElementFacade lbl15;
	@FindBy(xpath="//*[@id=\"id_sc_field_consignacion\"]")
	public WebElementFacade lbl16;
	@FindBy(xpath="//*[@id=\"id_sc_field_cancelacion_de_terceros\"]")
	public WebElementFacade lbl17;
	@FindBy(xpath="//*[@id=\"id_sc_field_depreciacion\"]")
	public WebElementFacade lbl18;
	@FindBy(xpath="//*[@id=\"id_sc_field_factura_de_kompra\"]")
	public WebElementFacade lbl19;
	@FindBy(xpath="//*[@id=\"id_sc_field_factura_de_wenta\"]")
	public WebElementFacade lbl20;
	@FindBy(xpath="//*[@id=\"id_sc_field_nota_bancaria\"]")
	public WebElementFacade lbl21;
	@FindBy(xpath="//*[@id=\"id_sc_field_nota_credito\"]")
	public WebElementFacade lbl22;
	@FindBy(xpath="//*[@id=\"id_sc_field_nota_debito\"]")
	public WebElementFacade lbl23;
	@FindBy(xpath="//*[@id=\"id_sc_field_reclasificacion\"]")
	public WebElementFacade lbl24;
	@FindBy(xpath="//*[@id=\"id_sc_field_saldos_iniciales\"]")
	public WebElementFacade lbl25;
	@FindBy(xpath="//*[@id=\"id_sc_field_documento_quivalente\"]")
	public WebElementFacade lbl26;
	@FindBy(xpath="//*[@id=\"id_sc_field_factura_de_servicio\"]")
	public WebElementFacade lbl27;
	
	
	@FindBy(xpath="//*[@id=\"sc_b_ins_t\"]")
	public WebElementFacade Btnguardar;
	@FindBy(xpath="//*[@id=\"sc_b_sai_t\"]")
	public WebElementFacade Btnregresar;
	@FindBy(xpath="//*[@id=\"tit_grid_sucursales__SCCS__1\"]/td[3]/a")
	public WebElementFacade Btnorganizar;
	@FindBy(xpath="//*[@id=\"tit_grid_sucursales__SCCS__1\"]/td[3]")
	public WebElementFacade Btnorganizar2;
	@FindBy(xpath="//*[@id=\"id_sc_field_descripcion_1\"]")
	public WebElementFacade lbldescripcion;
	@FindBy(id="item_380")
	public WebElementFacade Btnsalir;
	public void Descripcion(String Zimbra) {
		System.out.println("ingreso d");
		try {
			
			Txtdescripcion.sendKeys(Zimbra);
			System.out.println("ingreso datos d");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
	
		
	}
	public void AgregarPre(String Zimbra) {
		System.out.println("ingreso radio");
		try {
			
			Txtagre_prefijo.click();
			System.out.println("oprimio radio si");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
	public void Prefijo(String Zimbra) {
		
		Txtprefijo.sendKeys(Zimbra);
	}
	public void LNumero(String Zimbra) {
	
		TxtLNumero.sendKeys(Zimbra);
	}
	
	public void Btn_impresion() {
		System.out.println("ingreso menu 2");
		try {
			
			Btnimpresion.click();
			System.out.println("oprimio menu 2");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
	
	public void imagen1(String Zimbra) {
		System.out.println("ingreso icono imagen");
		try {
			
			Btnicon1.click();
			System.out.println("oprimio icon 1");
			Btnurl1.sendKeys(Zimbra);
			System.out.println("ingreso url 1");
			Btn1.click();
			System.out.println("guardo imagen 1");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
	public void imagen2(String Zimbra) {
		System.out.println("ingreso icono imagen 2"+ Zimbra);
		try {
			
			Btnicon2.click();
			System.out.println("oprimio icon 2");
			System.out.println(Zimbra);
			Btnurl2.sendKeys(Zimbra);
			System.out.println("ingreso url 2");
			Btn2.click();
			System.out.println("guardo imagen 2");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
	
	public void Btn_documentacion() {
		System.out.println("ingreso menu 3");
		try {			
			Btndocumentos.click();
			System.out.println("oprimio menu 3");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
	}
	
	public void documentacion(String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, String arg7, String arg8, String arg9, String arg10, String arg11, String arg12, String arg13, String arg14, String arg15, String arg16, String arg17, String arg18, String arg19, String arg20, String arg21, String arg22, String arg23, String arg24, String arg25, String arg26, String arg27) {
		System.out.println("ingreso vista documentación");
		try {
			
			lbl1.sendKeys(arg1);
			System.out.println("ingreso lbl1");
			lbl2.sendKeys(arg2);
			System.out.println("ingreso lbl2");
			lbl3.sendKeys(arg3);
			System.out.println("ingreso lbl3");
			lbl4.sendKeys(arg4);
			System.out.println("ingreso lbl4");
			lbl5.sendKeys(arg5);
			System.out.println("ingreso lbl5");
			lbl6.sendKeys(arg6);
			System.out.println("ingreso lbl1");
			lbl7.sendKeys(arg7);
			System.out.println("ingreso lbl7");
			lbl8.sendKeys(arg8);
			System.out.println("ingreso lbl8");
			lbl9.sendKeys(arg9);
			System.out.println("ingreso lbl9");
			lbl10.sendKeys(arg10);
			System.out.println("ingreso lbl10");
			lbl11.sendKeys(arg11);
			System.out.println("ingreso lbl11");
			lbl12.sendKeys(arg12);
			System.out.println("ingreso lbl12");
			lbl13.sendKeys(arg13);
			System.out.println("ingreso lbl13");
			lbl14.sendKeys(arg14);
			System.out.println("ingreso lbl14");
			lbl15.sendKeys(arg15);
			System.out.println("ingreso lbl15");
			lbl16.sendKeys(arg16);
			System.out.println("ingreso lbl16");
			lbl17.sendKeys(arg17);
			System.out.println("ingreso lbl17");
			lbl18.sendKeys(arg18);
			System.out.println("ingreso lbl18");
			lbl19.sendKeys(arg19);
			System.out.println("ingreso lbl19");
			lbl20.sendKeys(arg20);
			System.out.println("ingreso lbl20");
			lbl21.sendKeys(arg21);
			System.out.println("ingreso lbl21");
			lbl22.sendKeys(arg22);
			System.out.println("ingreso lbl22");
			lbl23.sendKeys(arg23);
			System.out.println("ingreso lbl23");
			lbl24.sendKeys(arg24);
			System.out.println("ingreso lbl24");
			lbl25.sendKeys(arg25);
			System.out.println("ingreso lbl25");
			lbl26.sendKeys(arg26);
			System.out.println("ingreso lbl26");
			lbl27.sendKeys(arg27);
			System.out.println("ingreso lbl27");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
	
	public void Btn_guardar() {
		System.out.println("ingreso a guardar");
		try {			
			Btnguardar.click();
			System.out.println("oprimio boton guardar");
			Btnregresar.click();
			System.out.println("ingreso a vista principal");
			
			Btnorganizar.click();
			System.out.println("organizo los registros");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
	}
	
	public void Validarsucursal(String descripcion) {
		try {
			System.out.println("ingreso al sistema de validación");
			
			Btnorganizar2.click();
			System.out.println("organizo los registros");
			String descrip=lbldescripcion.getText();
			assertEquals(descripcion,descrip);
			System.out.println("la descripción es igual, validación correcta");
		} catch (AssertionError e) {
			System.out.println("el error es " + e);
		}
		try {
			getDriver().switchTo().defaultContent();
				Btnsalir.click();
				System.out.println("Se cerro sesión de forma correcta");
		} catch (Exception e) {
			System.out.println("el error es " + e);
		}
		
	}
}
