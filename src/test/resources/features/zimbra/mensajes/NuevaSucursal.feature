@NuevaSucursal
Feature: Registrar una nueva sucursal

  @creacionsucursal
  Scenario Outline: Registrar nueva sucursal
    Given ingresar a la ruta de "<ruta>"
    And loguearse con otro usuario "<usuario1>" y otra contrasena "<pass1>"
    And loguearse con usuario "<usuario>" y contrasena "<pass>"
    When Ingresar al formulario de nueva sucursal
    And diligenciar el formulario de la vista general "<Descripcion>", "<prefijo_s_n>", "<Prefijo>", "<numero>"
    And diligenciar el formulario de la vista impresion "<imagen1>", "<imagen2>"
    And diligenciar el formulario de numeracion de documentos "<OC>", "<entrada>", "<FR>", "<DE>", "<DEC>", "<CDE>", "<Cotizacion>", "<pedido>", "<Remision>", "<FV>", "<DDV>", "<RDC>", "<Ajuste>", "<CC>", "<CDC>", "<Consignacion>", "<CDT>", "<Depreciacion>", "<FDC>", "<FDV>", "<NB>", "<NC>", "<ND>", "<reclasificacion>", "<SI>", "<DQ>", "<FDS>"
    And Guardar la nueva sucursal
    Then Validar la creacion de la nueva sucursal "<Descripcion>"

    Examples: 
      | ruta                       | usuario | pass   | Descripcion | prefijo_s_n | Prefijo | imagen1                                                                    | imagen2                                                                          | OC | entrada | FR | DE | DEC | CDE | Cotizacion | pedido | Remision | FV | DDV | RDC | Ajuste | CC | CDC | Consignacion | CDT | Depreciacion | FDC | FDV | NB | NC | ND | reclasificacion | SI | DQ | FDS | numero |usuario1|pass1|
      | https://app.extragerto.com/| admin   | master | prueba1     | Si          | 300     | https://www.fiibra.com/wp-content/uploads/2016/11/android-logo-200x200.jpg | https://dailymusicbreak.com/wp-content/uploads/2013/05/facebook-logo-200x200.png |  1 |       1 |  1 |  1 |   1 |   1 |          1 |      1 |        1 |  1 |   1 |   1 |      1 |  1 |   1 |            1 |   1 |            1 |   1 |   1 |  1 |  1 |  1 |               1 |  1 |  1 |   1 | 1|jbordat|Choucair.2018|
     
