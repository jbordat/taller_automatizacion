
@redactarmensaje
Feature: Redactar mensaje
  

  @rutacritica
  Scenario: redactar correo y validar al enviar
    Given ingresar al aplicativo zimbra "http://mx1.choucairtesting.com/"
    And loguearse con usuario "jbordat" y contraseña "Choucair.2018"
    When Ingresar al formulario de mensaje
    And diligenciar el formulario con encabezado "prueba1", cuerpo "mensaje prueba" y correo "jbordat@choucairtesting.com"
    And envio el mensaje
    Then validamos el envio con encabezado "prueba1", cuerpo "mensaje prueba" y correo "jbordat"

